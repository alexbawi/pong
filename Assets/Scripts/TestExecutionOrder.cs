﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestExecutionOrder : MonoBehaviour
{
    public int NumeroDeTest = 0;

    void Awake()
    {
        Debug.Log("Estoy en el Awake de TestExecutionOrder" + NumeroDeTest);
    }
    
    private void OnEable()
    {
        Debug.LogWarning("Activo TestExecutionOrder" + NumeroDeTest);
    }

    private void Start()
    {
        Debug.LogError("Activo TestExecutionOrder" + NumeroDeTest);
    }

}
