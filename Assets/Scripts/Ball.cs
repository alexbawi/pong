﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    private Vector2 ballPosition;
    public Vector2 ballVelocity;

    void Awake()
    {
        ballPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        ballPosition.y = transform.position.y + ballVelocity.y * Time.deltaTime;
        ballPosition.x = transform.position.x + ballVelocity.x * Time.deltaTime;

        transform.position = new Vector3(ballPosition.x, ballPosition.y, transform.position.z);

        if (ballPosition.y > 1.84)
        {
            ballVelocity.y = -ballVelocity.y;
        }

        if (ballPosition.y < -1.87)
        {
            ballVelocity.y = -ballVelocity.y;
        }

        if (ballPosition.x > 3.68)
        {
            ballVelocity.x = -ballVelocity.x;
        }

        if (ballPosition.x < -3.68)
        {
            ballVelocity.x = -ballVelocity.x;
        }


    }

}
